# Projekt BlueServiceDocMgr2
Projekt jest zrobiony w Eclipse Indigo.  
Można go uruchamiąć i testować korzystając z Eclipse.  
Główna klasa to DocMgr.java, testy w DocMgrTest.java.

Można tez uruchomić program z katalogu głownego projektu jako katalogu bieżacego (Java 7):
>  java -jar runnableDocMgr.jar  

Więcej w pliku projektu: doc/readme.txt.

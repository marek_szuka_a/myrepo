Projekt jest zrobiony w Eclipse Indigo.


Wiele rozwiązań wziąłem z systemu webowego HeyTaxi (który projektowałem)
i zaadoptowałem je do systemu desktopowego.

Projekt można uruchamiąć i testować korzystając z Eclipse.
Główna klasa to DocMgr.java, testy w DocMgrTest.java.

Można tez uruchomić program z katalogu głownego projektu jako katalogu bieżacego (Java 7):
  java -jar runnableDocMgr.jar
  
  
Składnia wywołań jest trochę sztuczna ale wziąłem z marszu bibliotekę:
  org.apache.commons.cli
do analizy składni argumentów i nie do końca ją czuję albo nie w pełni działa (a czasu brak).
  
Uwagi:
	- stronicowanie dotyczy ilości dokumentów i domyslnie jest 10 (mozna zmienic, patrz Const.java)
	- nie nalezy wprowadzić do tekstu dokumentu znaków:  :="  .
	- argumenty parametru (jeśli są konieczne) mogą być 1 lub 2 rozdzielone przecinkiem np:
		komenda filterList (arg=<name>:<state>) może być wołana:
			- f dom:CREATED
				lub
			  f :CREATED
			  	lub
			  f dom
	- testy sa zrobione dla 2 komend
	

Bazy danych to HSQLDB, produkcyjna i testowa są w: mydb.


	Marek Szukała
	tel 603 611 390
	
(2)	
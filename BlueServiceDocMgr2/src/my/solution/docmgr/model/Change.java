package my.solution.docmgr.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import my.solution.docmgr.main.State;

@Entity
@Table(name = "change")
public class Change extends AbstractModelBean{
    private static final long serialVersionUID= 1L;

    @Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	long id;

	private Integer state;
	public State getState(){
		return State.getByValue(state);
	}
	public void setState(State state){
		this.state = state.getValue();
	}
	
	@ManyToOne
	@JoinColumn(name="doc_id")
	Doc doc;
	public Doc getDoc() {
		return doc;
	}
	public void setDoc(Doc doc) {
		this.doc = doc;
	}
}	

package my.solution.docmgr.model;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import my.solution.docmgr.main.State;


@Entity
@Table(name = "doc")
public class Doc extends AbstractModelBean{
    private static final long serialVersionUID= 1L;

    @Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long id;
	
    @Column
    public String name;
	
	@Column(length=10000)
	public String content;

    @Column
    public String cause;

    @Column(name="unique_umber")
    public Long uniqueNumber;
    
	// current state
    @Column
	Integer state;
	public State getState(){
		return State.getByValue(state);
	}
	public void setState(State state){
		this.state = state.getValue();
	}
	
	@OneToMany(mappedBy="doc",fetch=FetchType.EAGER)
	Collection<Change> changes;
	public Collection<Change> getChanges() {
		return changes;
	}
	public void setChanges(Collection<Change> changes) {
		this.changes = changes;
	}
}	

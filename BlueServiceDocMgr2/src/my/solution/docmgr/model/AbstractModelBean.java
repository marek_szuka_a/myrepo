package my.solution.docmgr.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public abstract class AbstractModelBean implements Serializable{
	private static final long serialVersionUID = 1L;
    
    public boolean equals( Object o ){
        return EqualsBuilder.reflectionEquals(this, o);
    }
    public int hashCode(){
        return HashCodeBuilder.reflectionHashCode(this);
    }
}

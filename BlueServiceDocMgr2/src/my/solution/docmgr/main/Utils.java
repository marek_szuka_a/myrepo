package my.solution.docmgr.main;

import java.util.Date;
import java.util.Random;

public class Utils {
	static private Random random = new Random(new Date().getTime());
	static private String allowedChars = "0123456789";
	
	public synchronized static String create(){
		StringBuffer sb = new StringBuffer();
		for(int i=0; i<9; i++){
			int j = Math.abs(random.nextInt()) % allowedChars.length();
			sb.append(allowedChars.charAt(j));
		}
		return sb.toString();
	}
	
	static public void sleep1sec(){
		sleepInSec(1);
	}
	static public void sleepInSec(int timeInSeconds){
		try { Thread.sleep(1000l * timeInSeconds); }
		catch(InterruptedException ignored){}
	}

	static public Long parToLong(String s){
		try{
			return Long.parseLong(s);
		}catch(Exception e){ }
		return null;
	}
	static public Integer parToInt(String s){
		try{
			return Integer.parseInt(s);
		}catch(Exception e){ }
		return null;
	}
	static public State parToState(String s){
		try{
			return State.valueOf(s);
		}catch(Exception e){ }
		return null;
	}
}

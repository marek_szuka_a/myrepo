package my.solution.docmgr.main;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Marek Szukala
 * 
 * Return value of operations
 *
 */
public enum OpeRet{
	OK(0),
	NO_DOCUMENT(1),
	OPERATION_NOT_ALLOWED(2),
	BAD_ARGUMENT(3),
	NO_PAGE_NUMBER(4),
	BAD_STATE_ARGUMENT(5),
	ONLY_ONE_ARGUMENT_ALLOWED(5),
	CAUSE_ARGUMENT_NEEDED(5);
	
	private final Integer value;  
	private OpeRet(Integer value) {  
		this.value = value;  
	}  
	public Integer getValue() {  
		return value;  
	}  

	static public OpeRet getByValue(Integer value){
		return map.get(value);
	}

	private static final Map<Integer, OpeRet> map = new HashMap<Integer, OpeRet>();
	static{
	  for (OpeRet item : values()){
	    map.put(item.getValue(), item);
	  }
	}	
}

package my.solution.docmgr.main;

import static java.lang.System.out;

import java.util.List;

import my.solution.docmgr.dao.ChangeDao;
import my.solution.docmgr.dao.DocDao;
import my.solution.docmgr.model.Change;
import my.solution.docmgr.model.Doc;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Component
public class Service {
	final Log log = LogFactory.getLog(this.getClass());
	
	final static int MAX_FIELD_LENGTH = 255; 
	final static int MAX_CONTENT_LENGTH = 10000; 

	@Autowired private DocDao docDao;
	@Autowired private ChangeDao changeDao;

	@Transactional
	public Doc create(String name,String content){
		State state = State.CREATED;
		
		Doc doc = new Doc();
		doc.name = limitStringSize(name,MAX_FIELD_LENGTH);
		doc.content = limitStringSize(content,MAX_CONTENT_LENGTH);
		doc.setState(state);
		doc = docDao.update(doc);
		
		addChange(state,doc);
		return doc;
	}
	
	@Transactional
	public OpeRet verify(Long docId,String content){
		Doc doc = docDao.findById(docId);
		if(doc == null) return OpeRet.NO_DOCUMENT;
		
		if(!doc.getState().equals(State.CREATED)) return OpeRet.OPERATION_NOT_ALLOWED;
		State state = State.VERIFIED;

		if(content != null) doc.content = limitStringSize(content,MAX_CONTENT_LENGTH);
		doc.setState(state);
		doc = docDao.update(doc);
		
		addChange(state,doc);
		return OpeRet.OK;
	}
	
	@Transactional
	public OpeRet accept(Long docId){
		Doc doc = docDao.findById(docId);
		if(doc == null) return OpeRet.NO_DOCUMENT;

		if(!doc.getState().equals(State.VERIFIED)) return OpeRet.OPERATION_NOT_ALLOWED;
		State state = State.ACCEPTED;

		doc.setState(state);
		doc = docDao.update(doc);
		
		addChange(state,doc);
		return OpeRet.OK;
	}

	@Transactional
	public OpeRet publish(Long docId){
		Doc doc = docDao.findById(docId);
		if(doc == null) return OpeRet.NO_DOCUMENT;

		if(!doc.getState().equals(State.ACCEPTED)) return OpeRet.OPERATION_NOT_ALLOWED;
		State state = State.PUBLISHED;

		doc.setState(state);
		doc.uniqueNumber = createUniqueNumber();
		doc = docDao.update(doc);
		
		addChange(state,doc);
		return OpeRet.OK;
	}

	@Transactional
	public Doc show(Long docId){
		return docDao.findById(docId);
	}

	@Transactional
	public OpeRet delete(Long docId,String cause){
		Doc doc = docDao.findById(docId);
		if(doc == null) return OpeRet.NO_DOCUMENT;

		if(!doc.getState().equals(State.CREATED)) return OpeRet.OPERATION_NOT_ALLOWED;
		State state = State.DELETED;

		doc.setState(state);
		doc.cause = limitStringSize(cause,MAX_FIELD_LENGTH);
		doc = docDao.update(doc);
		
		addChange(state,doc);
		return OpeRet.OK;
	}

	@Transactional
	public OpeRet reject(Long docId,String cause){
		Doc doc = docDao.findById(docId);
		if(doc == null) return OpeRet.NO_DOCUMENT;

		if(!(doc.getState().equals(State.ACCEPTED) || doc.getState().equals(State.VERIFIED))) return OpeRet.OPERATION_NOT_ALLOWED;
		State state = State.REJECTED;

		doc.setState(state);
		doc.cause = limitStringSize(cause,MAX_FIELD_LENGTH);
		doc = docDao.update(doc);
		
		addChange(state,doc);
		return OpeRet.OK;
	}

	public String docToString(Doc doc){
		if(doc == null) return "";
		String s = String.format("Doc -> id=%d name=%s content=%s state=%s",doc.id,doc.name,doc.content,doc.getState());
		if(doc.uniqueNumber != null) s += " uniqueNumber="+doc.uniqueNumber;
		if(doc.cause != null) s += " cause="+doc.cause;
		if(doc.getChanges() != null){
			s += " changes=";
			for(Change change : doc.getChanges()){
				s += change.getState().toString()+" ";
			}
		}
		return s;
	}
	
	public List<Doc> findAll(){
		return docDao.findAll(null);
	}
	public void listAll(Integer page){
		List<Doc> list = docDao.findAll(page);
		//out.println("@ list.size="+list.size());
		for(Doc ele : list){
			out.println(docToString(ele));
		}
	}
	public void listByName(String name,Integer page){
		List<Doc> list = docDao.findByName(name, page);
		//out.println("@ list.size="+list.size());
		for(Doc ele : list){
			out.println(docToString(ele));
		}
	}
	public void listByNameAndState(String name,State state){
		if(name != null && name.length() == 0) name = null;
		List<Doc> list = docDao.findByNameAndState(name, state);
		//out.println("@ list.size="+list.size());
		for(Doc ele : list){
			out.println(docToString(ele));
		}
	}

	@Transactional
	public void deleteAll(){
		changeDao.deleteAll();
		docDao.deleteAll();
	}

	// helpers
	private void addChange(State state,Doc doc){
		Change change = new Change();
		change.setState(state);
		change.setDoc(doc);
		changeDao.update(change);
	}
	
	private Long createUniqueNumber(){
		for(int i = 0; i<3; i++){
			String s = Utils.create();
			Long lo = Long.parseLong(s);

			List<Doc> list = docDao.findByProperty(null,"uniqueNumber",lo,0,1);
			if(list.size() == 0) return lo;
		}

		log.error("could not generate new access code value");
		return null;
	}
	
	private String limitStringSize(String s,int maxLength){
		if(s == null) return s;
		if(s.length() <= maxLength) return s;
		return s.substring(0, maxLength-3) +"...";
	}
}

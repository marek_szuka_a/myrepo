package my.solution.docmgr.main;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class DocMgrTest {
	static DocMgr dm = null;
	ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	String[] args;
	String s,value;
	int count;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		dm = new DocMgr(true);
	}
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		dm.close();
	}

	@Before
	public void setUp() throws Exception {
		System.setOut(new PrintStream(outContent));
		count = 0;
	}
	@After
	public void tearDown() throws Exception {
		System.setOut(null);
	}

	@Test
	public void testCreate() {
		//System.err.println("---create");
		executeCommand(new String [] {"-create house:\"This is house\""});
		assertTrue(s.startsWith("OK : create"));
		value = getValueFromOption(s,"id");
		
		// there is one record
		assertEquals(1,dm.getService().findAll().size());
		
		// checking content
		executeCommand(new String [] {"-s",value});
		assertTrue(s.contains("name=house"));
		assertTrue(s.contains("content=This is house"));

		// creating second record
		executeCommand(new String [] {"-c cat:\"This is cat\""});
		assertTrue(s.startsWith("OK : create"));
		value = getValueFromOption(s,"id");
		
		// there are two records
		assertEquals(2,dm.getService().findAll().size());

		// checking content
		executeCommand(new String [] {"-s",value});
		assertTrue(s.contains("name=cat"));
		assertTrue(s.contains("content=This is cat"));
	}

	@Test
	public void testList() {
		//System.err.println("---list");
		executeCommand(new String [] { "-l" });
		assertTrue(s.startsWith("OK : list"));
		assertTrue(s.contains("name=house"));
		assertTrue(s.contains("name=cat"));
		
		// there are two records
		assertEquals(2,dm.getService().findAll().size());
		
		// deleting all
		dm.getService().deleteAll();

		// adding count records"
		int count = 100;
		for(int i=0; i<count; i++){
			executeCommand(new String [] {"-create mew:Bird"+i});
		}

		// there are count records
		assertEquals(count,dm.getService().findAll().size());

		// testing whether all records are present
		executeCommand(new String [] { "-l" } , "list all");
		int i = 0;
		for( ; i<count; i++){
			assertTrue(s.contains("content=Bird"+i));
		}
		assertTrue(! s.contains("content= "));
		assertTrue(! s.contains("content=null"));
		assertTrue(! s.contains("content=Bird "));
		assertTrue(! s.contains("content=Bird"+ (i+1)));

		// list page
		executeCommand(new String [] { "-L", "0"});
		assertTrue(s.startsWith("OK : ListPage"));
		
		// content of the first page
		assertTrue(s.contains("content=Bird0"));
		assertTrue(s.contains("content=Bird"+(Const.PAGE_COUNT-1)));
		assertTrue(! s.contains("content=Bird"+Const.PAGE_COUNT));

		// content of the second page
		executeCommand(new String [] { "-L", "1"});
		assertTrue(s.startsWith("OK : ListPage"));
		
		assertTrue(s.contains("content=Bird"+Const.PAGE_COUNT));
		assertTrue(s.contains("content=Bird"+(2 * Const.PAGE_COUNT-1)));
		assertTrue(! s.contains("content=Bird"+(2 * Const.PAGE_COUNT)));
	}
	
	@Test
	public void toBeContinued() {
		assertTrue(true);
	}
	
	// helpers
	void executeCommand(String[] args,String...comment){
		outContentReset();
		s = parseAndGetOutput(args);
		if(comment.length == 1) System.err.print(comment[0]+" // ");
		testPrintS();
	}
	String parseAndGetOutput(String[] args){
		this.args = args;
		dm.parse(args);
		return outContent.toString();
	}
	String getValueFromOption(String s,String name){
		Pattern pattern = Pattern.compile(name+"=(\\S*)");
		Matcher matcher = pattern.matcher(s);
		if (matcher.find()) return matcher.group(1);
		return "";
	}
	void outContentReset(){
		outContent.reset();
	}
	void testPrintS(){
		System.err.println("@"+count++ + " s="+s);
	}
}

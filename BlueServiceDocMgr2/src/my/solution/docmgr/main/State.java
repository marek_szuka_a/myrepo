package my.solution.docmgr.main;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Marek Szukala
 *
 */
public enum State{
	CREATED(0),
	VERIFIED(1),
	ACCEPTED(2),
	PUBLISHED(3),
	DELETED(4),
	REJECTED(5);
	
	private final Integer value;  
	private State(Integer value) {  
		this.value = value;  
	}  
	public Integer getValue() {  
		return value;  
	}  

	static public State getByValue(Integer value){
		return map.get(value);
	}

	private static final Map<Integer, State> map = new HashMap<Integer, State>();
	static{
	  for (State item : values()){
	    map.put(item.getValue(), item);
	  }
	}	
}

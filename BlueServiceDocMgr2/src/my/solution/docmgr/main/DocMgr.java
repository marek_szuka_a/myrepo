package my.solution.docmgr.main;

import static java.lang.System.out;
import my.solution.docmgr.model.Doc;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class DocMgr {
	final String contextFile = "META-INF/applicationContext.xml";
	final String contextFileForTest = "META-INF/applicationContextTest.xml";
	
	static final String programName = "DocMgr";
	
	ApplicationContext context = null;
	
	@Autowired private Service service;
	
	public DocMgr(boolean forTest){
		context = new ClassPathXmlApplicationContext(forTest ? contextFileForTest : contextFile);
		AutowireCapableBeanFactory acbFactory = context.getAutowireCapableBeanFactory();
		acbFactory.autowireBean(this);
	}
	
	public void close(){
		Utils.sleepInSec(2);
		((AbstractApplicationContext)context).registerShutdownHook();
	}

	// to facilitate testing
	public Service getService(){
		return service;
	}
	
	void parse(String[] args){
		CommandLineParser parser = new DefaultParser();
		Options options = new Options();
		
		options.addOption("h", "help", false, "show help.");
		options.addOption("l", "list", false, "show list.");
		options.addOption("L", "ListPage", true, "show list page arg=<page (from 0)>.");
		options.addOption("f", "filterList", true, "show filtered list arg=<name>:<state>.");
		options.addOption("s", "show", true, "show arg=<id>.");
		options.addOption("c", "create", true, "create Document arg=<name>:\"<content>\".");
		options.addOption("v", "verify", true, "verify Document arg=<id>:\"<content>\".");
		options.addOption("a", "accept", true, "accept Document arg=<id>.");
		options.addOption("p", "publish", true, "publish Document arg=<id>.");
		options.addOption("d", "delete", true, "delete Document arg=<id>:\"<cause>\".");
		options.addOption("r", "reject", true, "reject Document arg=<id>:\"<cause>\".");
		

	    try {
		    // parse the command line arguments
		    CommandLine line = parser.parse(options, args );
		    
		    if(line.getOptions().length == 0 || line.hasOption("h")){
				HelpFormatter formater = new HelpFormatter();
				formater.printHelp(programName, options);
		    }

		    if(line.hasOption("l")){
		    	String ope = "list";
				out.println(printRet(ope,OpeRet.OK));
		    	service.listAll(null);
		    }
		    
		    if(line.hasOption("L")){
		    	String ope = "ListPage";

		    	String [] ts = parse2params(line.getOptionValue("L"));
		    	if(ts == null || Utils.parToInt(ts[0]) == null) out.println(printRet(ope,OpeRet.NO_PAGE_NUMBER));
		    	else{
		    		out.println(printRet(ope,OpeRet.OK));
		    		service.listAll(Utils.parToInt(ts[0]));
		    	}
		    }

		    if(line.hasOption("f")){
		    	String ope = "listFilter";

		    	String [] ts = parse2params(line.getOptionValue("f"));
		    	if(ts == null) out.println(printRet(ope,OpeRet.BAD_ARGUMENT));
		    	else if(ts[1] != null && Utils.parToState(ts[1]) == null) out.println(printRet(ope,OpeRet.BAD_STATE_ARGUMENT));
		    	else{
					out.println(printRet(ope,OpeRet.OK));
					service.listByNameAndState(ts[0],Utils.parToState(ts[1]));
		    	}
		    }
		    
		    if(line.hasOption("s")){
		    	String ope = "show";
		    	String [] ts = parse2params(line.getOptionValue("s"));
		    	if(ts == null || ts[1] != null) out.println(printRet(ope,OpeRet.ONLY_ONE_ARGUMENT_ALLOWED));
		    	else{
		    		OpeRet opeRet = OpeRet.OK;
		    		Doc doc = service.show(Utils.parToLong(ts[0]));
					if(doc == null) opeRet = OpeRet.NO_DOCUMENT;
					out.println(printRet(ope,opeRet));
					if(doc != null) out.println(service.docToString(doc));
		    	}
		    }

		    if(line.hasOption("c")){
		    	String ope = "create";
		    	String [] ts = parse2params(line.getOptionValue("c"));
		    	if(ts == null)  out.println(printRet(ope,OpeRet.BAD_ARGUMENT));
		    	else{
					Doc doc = service.create(ts[0],ts[1]);
					out.println(printRet(ope,OpeRet.OK));
					out.println(service.docToString(doc));
		    	}
		    }
		    
		    if(line.hasOption("v")){
		    	String ope = "verify";
		    	String [] ts = parse2params(line.getOptionValue("v"));
		    	if(ts == null)  out.println(printRet(ope,OpeRet.BAD_ARGUMENT));
		    	else{
		    		OpeRet opeRet = service.verify(Utils.parToLong(ts[0]), ts[1]);
					printDoc(ope,ts[0],opeRet);
		    	}
		    }

		    if(line.hasOption("a")){
		    	String ope = "accept";
		    	String [] ts = parse2params(line.getOptionValue("a"));
		    	if(ts == null || ts[1] != null) out.println(printRet(ope,OpeRet.BAD_ARGUMENT));
		    	else{
					OpeRet opeRet = service.accept(Utils.parToLong(ts[0]));
					printDoc(ope,ts[0],opeRet);
		    	}
		    }

		    if(line.hasOption("p")){
		    	String ope = "publish";
		    	String [] ts = parse2params(line.getOptionValue("p"));
		    	if(ts == null || ts[1] != null) out.println(printRet(ope,OpeRet.BAD_ARGUMENT));
		    	else{
					OpeRet opeRet = service.publish(Utils.parToLong(ts[0]));
					printDoc(ope,ts[0],opeRet);
		    	}
		    }

		    if(line.hasOption("d")){
		    	String ope = "delete";
		    	String [] ts = parse2params(line.getOptionValue("d"));
		    	if(ts[1] == null) out.println(printRet(ope,OpeRet.CAUSE_ARGUMENT_NEEDED));
		    	else if(ts == null || ts[1] == null) out.println(printRet(ope,OpeRet.BAD_ARGUMENT));
		    	else{
					OpeRet opeRet = service.delete(Utils.parToLong(ts[0]),ts[1]);
					printDoc(ope,ts[0],opeRet);
		    	}
		    }

		    if(line.hasOption("r")){
		    	String ope = "reject";
		    	String [] ts = parse2params(line.getOptionValue("r"));
		    	if(ts[1] == null) out.println(printRet(ope,OpeRet.CAUSE_ARGUMENT_NEEDED));
		    	else if(ts == null || ts[1] == null) out.println(printRet(ope,OpeRet.BAD_ARGUMENT));
		    	else{
					OpeRet opeRet = service.reject(Utils.parToLong(ts[0]),ts[1]);
					printDoc(ope,ts[0],opeRet);
		    	}
		    }
	    }
		catch( ParseException exp ) {
		    out.println( "*** error: " + exp.getMessage() );
		}		
	}
	
	// helpers
	String [] parse2params(String s){
		if(s == null) return null;
		String [] tsr = new String[2];
		String [] ts = s.split(":");
		if(ts.length >= 1) tsr[0] = ts[0].replaceAll("\\\"","").trim();
		if(ts.length == 2) tsr[1] = ts[1].replaceAll("\\\"","");
		if(ts.length >= 1 && ts.length <= 2) return tsr;
		return null;
	}
	String printRet(String ope,OpeRet opeRet){
		String s = "";
		if(!opeRet.equals(OpeRet.OK)) s = "*** ";
		s += opeRet;
		s += " : "+ope;
		return s;
	}
	void printDoc(String ope,String id, OpeRet opeRet){
		out.println(printRet(ope,opeRet));
		if(opeRet.equals(OpeRet.OK)){
			Doc doc = service.show(Utils.parToLong(id));
			out.println(service.docToString(doc));
		}
	}
	

	public static void main(String[] args) {
		out.println("-- "+programName+" --");
		
		//out.println("@ args.length="+args.length);
		//for(String s : args) out.println("@    ="+s);
		
		/*args = new String [] {
			//"-s 1",
			//"-l", "66",
			"-L", "0",
			//"-f :CREATED",
			//"-create Biesy:\"Powie�� Dostojewskiego\"",
			//"-verify 21:\"Powie�� Dostojewskiego, hej\"",
			//"-accept 21",
			//"-publish 21",
			//"-delete 4",
			//"-reject 25:\"Odrzucamy jednak\"",
		};*/
		DocMgr dm = new DocMgr(false);
		dm.parse(args);

		//dm.service.listAll(); // test
		dm.close();
	}
}

package my.solution.docmgr.dao;

import my.solution.docmgr.model.Change;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Marek Szukala
 *
 */
@Repository("changeDao")
public class ChangeDao extends BasicDao<Change>{

	public ChangeDao() {
		super(Change.class); // We pass class for parent
	}

	public void deleteAll(){
		_deleteAll();
	}

	@Transactional
	public Change update(Change change){
		return _update(change);
	}
}

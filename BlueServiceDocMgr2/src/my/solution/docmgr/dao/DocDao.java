package my.solution.docmgr.dao;

import java.util.List;

import javax.persistence.Query;

import my.solution.docmgr.main.Const;
import my.solution.docmgr.main.State;
import my.solution.docmgr.model.Doc;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Marek Szukala
 *
 */
@Repository("docDao")
public class DocDao extends BasicDao<Doc>{

	public DocDao() {
		super(Doc.class); // We pass class for parent
	}

	public void deleteAll(){
		_deleteAll();
	}

	public List<Doc> findAll(Integer page){
		Integer [] tic = pageToIdxCount(page,Const.PAGE_COUNT);
		if(tic != null) return _findAll("id",tic[0],tic[1]);
		return _findAll("id");
	}
	
	public List<Doc> findByName(String name,Integer page){
		Integer [] tic = pageToIdxCount(page,Const.PAGE_COUNT);

		if(tic != null) return findByProperty("id","name",name,tic[0],tic[1]);
		return findByProperty("id","name",name);
	}
	
	public List<Doc> findByNameAndState(String name,State state){
		String queryPartName = "";
		if(name != null) queryPartName = " AND doc.name = :name";
		
		String queryPartState = "";
		if(state != null) queryPartState = " AND doc.state = :state";

		String queryString = "select doc from Doc doc where"+
			" 2 = 2" +
			queryPartName+queryPartState+
			" order by id";
		
		Query query = em.createQuery(queryString);
		if(queryPartName.length() > 0) query.setParameter("name",name);
		if(queryPartState.length() > 0) query.setParameter("state",state.getValue());
		
		return query.getResultList();
	}

	public Doc findById(Long docId){
		if(docId == null) return null;
		return _findById(docId);
	}

	@Transactional
	public Doc update(Doc doc){
		return _update(doc);
	}

	@Transactional
	public void save(Doc doc){
		_save(doc);
	}
	
	// helper
	Integer [] pageToIdxCount(Integer page,Integer pageCount){
		if(page != null && page >= 0){
			return new Integer[] { page * pageCount, pageCount};
		}
		return null;
	}
}

package my.solution.docmgr.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.transaction.annotation.Transactional;


/**
 * BasicDAO
 * 
 * Not catched RuntimeException means that the transaction is aborted.
 *
 * @param <T>
 */
public class BasicDao<T> {
	// we get this way: T.class
	// I can't do it easier
	private Class<T> returnedClass = null;
    protected BasicDao(Class<T> returnedClass) {
      this.returnedClass = returnedClass;
    }
    public Class returnedClass() {
      return returnedClass;
    }	
	
    // log
    final Log log = LogFactory.getLog(this.getClass());


    @PersistenceContext
	EntityManager em;

    
	public void flush(){
		log.debug("flush");
		em.flush();
	}

	@Transactional
	public void _save(T ed){
		log.debug("store");
		em.persist(ed);
	}

	@Transactional
	public T _update(T ed){
		log.debug("update");
		T ed1 = em.merge(ed);
		return ed1;
	}
	
	@Transactional
	public void delete(Long id){
		T ed = em.find(returnedClass,id);
		log.debug("delete , id="+id);
		em.remove(ed);
	}

	@Transactional
	public void _deleteAll(){
		final String queryString = "delete from "+returnedClass.getSimpleName()+" x";
		Query query = em.createQuery(queryString);
		int count = query.executeUpdate();
		log.debug("deleteAll ("+count+")");
	}
	
	@Transactional
	public void deleteByProperty(String propertyName,final Object value){
		String wherePart = "";
		if(propertyName != null && !propertyName.equals("")){
			wherePart =  " where x."+propertyName + "= :propertyValue";
		}

		final String queryString = "delete from "+returnedClass.getSimpleName()+" x"+wherePart;
		Query query = em.createQuery(queryString);
		if(wherePart.length() > 0)
			query.setParameter("propertyValue", value);
		int count = query.executeUpdate();
		log.debug("deleteByProperty ("+count+")");
	}
	
	@Transactional(readOnly=true)
	public T _findById(Long id){
		T ed = em.find(returnedClass,id);
		log.debug("findById , id="+id);
		return ed;
	}

	@Transactional(readOnly=true)
	public List<T> _findAll(String sortField,final int...rowStartIdxAndCount){
		return findByProperty(sortField,null,null,rowStartIdxAndCount);
	}

	@Transactional(readOnly=true)
	public Long countAll(){
		final String queryString = "SELECT count(*) FROM "+returnedClass.getSimpleName()+" x";
		Query query = em.createQuery(queryString);
		return ((Long)(query.getSingleResult())).longValue();
	}
	
	@Transactional(readOnly=true)
	public List<T> findByProperty(String sortField,String propertyName,final Object value,final int...rowStartIdxAndCount){

		String [] ret = orderHelper(sortField);
		sortField = ret[0];
		String order = ret[1];
		
		String wherePart = "";
		if(propertyName != null && !propertyName.equals("")){
			wherePart =  " where x."+propertyName + "= :propertyValue";
		}

		final String queryString = 
			"SELECT x FROM "+returnedClass.getSimpleName()+" x"+
			wherePart+
			(sortField==null ? "" : " ORDER BY x."+sortField)+
			(order==null ? "" : order);

		Query query = em.createQuery(queryString);
		if(wherePart.length() > 0)
			query.setParameter("propertyValue", value);
		
		String s = rowStartIdxAndCountHelper(query,rowStartIdxAndCount);
		log.debug("findByProperty, queryString"+s+"= "+queryString);
		
		return query.getResultList();
	}
	
	// helpers
	String rowStartIdxAndCountHelper(Query query,final int...rowStartIdxAndCount){
		if (rowStartIdxAndCount != null && rowStartIdxAndCount.length > 0) {	
			int rowStartIdx = Math.max(0,rowStartIdxAndCount[0]);
			int rowCount = -1;
			if (rowStartIdx > 0) {
				query.setFirstResult(rowStartIdx);
			}
			if (rowStartIdxAndCount.length > 1) {
				rowCount = Math.max(0,rowStartIdxAndCount[1]);
				if (rowCount > 0) {
					query.setMaxResults(rowCount);    
				}
			}
			return "("+rowStartIdx+","+rowCount+")";
		}										
		return "";
	}
	String[] orderHelper(String sortField){
		if(sortField != null && sortField.equals("")) sortField = null;
		String order = null;
		if(sortField != null){
			if(sortField.startsWith("+")) sortField = sortField.substring(1);
			else{
				if(sortField.startsWith("-")){
					sortField = sortField.substring(1);
					order = " desc";
				}
			}
		}
		return new String[] { sortField, order};
	}
	
	String errorMessage(Exception e){
		String s= e.getClass().getSimpleName();
		if(e.getMessage() != null)
			s += " "+e.getMessage();
		return s;
	}
}
